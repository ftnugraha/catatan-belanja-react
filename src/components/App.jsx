import { useState } from "react";
import Header from "./Header";
import Form from "./Form";
import GroceryList from "./GroceryList";
import Footer from "./Footer";

const groceryItems = [
  {
    id: 1,
    name: "Kopi Bubuk",
    quantity: 5,
    checked: true,
  },
  {
    id: 2,
    name: "Gula Pasir",
    quantity: 2,
    checked: false,
  },
  {
    id: 3,
    name: "Air Mineral",
    quantity: 1,
    checked: true,
  },
  {
    id: 4,
    name: "Teh Kotak",
    quantity: 4,
    checked: false,
  },
];

export default function App() {
  //Lifting state up
  const [items, setItems] = useState(groceryItems);

  function handleAddItem(item) {
    setItems([...items, item]);
  }

  function handleDeleteItem(id) {
    //hanya menampilkan items yang id nya != id (yang dihapus)
    setItems((items) => items.filter((item) => item.id !== id));
  }

  function handleToggleItem(id) {
    setItems((items) =>
      items.map((item) =>
        item.id === id ? { ...item, checked: !item.checked } : item
      )
    );
  }

  function handleClearItems() {
    setItems([]);
  }

  return (
    <div className="app">
      <Header />
      <Form onAddItem={handleAddItem} />
      <GroceryList
        items={items}
        onDeleteItem={handleDeleteItem}
        onToggleItem={handleToggleItem}
        onClearItems={handleClearItems}
      />
      <Footer items={items} />
    </div>
  );
}
